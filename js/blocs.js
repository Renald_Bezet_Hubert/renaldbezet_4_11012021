/* eslint-disable no-nested-ternary */
/* eslint-disable eqeqeq */
/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
/* eslint-disable no-sequences */
/* eslint-disable no-shadow */
/* eslint-disable no-unused-expressions */
// Blocs.js Minified
function setUpSpecialNavs() {
  const newLocal = '.navbar-toggle'
  // eslint-disable-next-line no-unused-expressions
  // eslint-disable-next-line no-undef
  $(newLocal).click(function (t) {
    // eslint-disable-next-line no-undef
    const e = $(this).closest('nav')
    const i = e.find('ul.site-navigation')
    const a = i.clone()
    if (i.parent().hasClass('nav-special')) {
      // eslint-disable-next-line no-undef
      // eslint-disable-next-line no-sequences
      // eslint-disable-next-line no-undef
      if ((t.stopPropagation(), $(this).hasClass('selected-nav'))) {
        $('.blocsapp-special-menu blocsnav').removeClass('open'),
          $('.selected-nav').removeClass('selected-nav'),
          setTimeout(() => {
            $('.blocsapp-special-menu').remove(),
              $('body').removeClass('lock-scroll'),
              $('.selected-nav').removeClass('selected-nav')
          }, 300)
      } else {
        $(this).addClass('selected-nav')
        const o = e.attr('class').replace('navbar', '').replace('row', '')
        const l = i
          .parent()
          .attr('class')
          .replace('navbar-collapse', '')
          .replace('collapse', '')
        ;($('.content-tint').length = -1) &&
          $('body').append('<div class="content-tint"></div>'),
          a
            .insertBefore('.page-container')
            .wrap(
              `<div class="blocsapp-special-menu ${o}"><blocsnav class="${l}">`
            ),
          $('blocsnav').prepend(
            '<a class="close-special-menu animated fadeIn" style="animation-delay:0.5s;"><div class="close-icon"></div></a>'
          ),
          (function () {
            let t = 'fadeInRight'
            let e = 0
            let i = 60
            $('.blocsapp-special-menu blocsnav').hasClass('fullscreen-nav')
              ? ((t = 'fadeIn'), (i = 100))
              : $('.blocsapp-special-menu').hasClass('nav-invert') &&
                (t = 'fadeInLeft')
            $('.blocsapp-special-menu blocsnav li').each(function () {
              $(this).parent().hasClass('dropdown-menu')
                ? $(this).addClass('animated fadeIn')
                : ((e += i),
                  $(this)
                    .attr('style', `animation-delay:${e}ms`)
                    .addClass(`animated ${t}`))
            })
          })(),
          setTimeout(() => {
            $('.blocsapp-special-menu blocsnav').addClass('open'),
              $('.content-tint').addClass('on'),
              $('body').addClass('lock-scroll')
          }, 10)
      }
    }
  }),
    $('body')
      .on('mousedown touchstart', '.content-tint, .close-special-menu', (t) => {
        $('.content-tint').removeClass('on'),
          $('.selected-nav').click(),
          setTimeout(() => {
            $('.content-tint').remove()
          }, 10)
      })
      .on('click', '.blocsapp-special-menu a', (t) => {
        $(t.target).closest('.dropdown-toggle').length ||
          $('.close-special-menu').mousedown()
      })
}

function extraNavFuncs() {
  $('.site-navigation a').click((t) => {
    $(t.target).closest('.dropdown-toggle').length ||
      $('.navbar-collapse').collapse('hide')
  }),
    $('a.dropdown-toggle').click(function (t) {
      $(this).parent().addClass('target-open-menu'),
        $(this)
          .closest('.dropdown-menu')
          .find('.dropdown.open')
          .each(function (t) {
            $(this).hasClass('target-open-menu') || $(this).removeClass('open')
          }),
        $('.target-open-menu').removeClass('target-open-menu')
    })
}

function setFillScreenBlocHeight() {
  $('.bloc-fill-screen').each(function (t) {
    const e = $(this)
    ;(window.fillBodyHeight = 0),
      $(this)
        .find('.container')
        .each(function (t) {
          ;(fillPadding = 2 * parseInt($(this).css('padding-top'))),
            e.hasClass('bloc-group')
              ? (fillBodyHeight = fillPadding + $(this).outerHeight() + 50)
              : (fillBodyHeight =
                  fillBodyHeight + fillPadding + $(this).outerHeight() + 50)
        }),
      $(this).css('height', `${getFillHeight()}px`)
  })
}

function getFillHeight() {
  let t = $(window).height()
  return t < fillBodyHeight && (t = fillBodyHeight + 100), t
}

function scrollToTarget(t) {
  t == 1
    ? (t = 0)
    : t == 2
    ? (t = $(document).height())
    : ((t = $(t).offset().top),
      $('.sticky-nav').length &&
        (t -= $('.sticky-nav .navbar-header').height())),
    $('html,body').animate(
      {
        scrollTop: t,
      },
      'slow'
    ),
    $('.navbar-collapse').collapse('hide')
}

function animateWhenVisible() {
  hideAll(),
    inViewCheck(),
    $(window).scroll(() => {
      inViewCheck(), scrollToTopView(), stickyNavToggle()
    })
}

function setUpDropdownSubs() {
  $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function (t) {
    t.preventDefault(),
      t.stopPropagation(),
      $(this).parent().siblings().removeClass('open'),
      $(this).parent().toggleClass('open')
    const e = $(this).parent().children('.dropdown-menu')
    e.offset().left + e.width() > $(window).width() &&
      e.addClass('dropmenu-flow-right')
  })
}

function stickyNavToggle() {
  let t = 0
  let e = 'sticky'
  if ($('.sticky-nav').hasClass('fill-bloc-top-edge')) {
    let i = $('.fill-bloc-top-edge.sticky-nav').parent().css('background-color')
    i == 'rgba(0, 0, 0, 0)' && (i = '#FFFFFF'),
      $('.sticky-nav').css('background', i),
      (t = $('.sticky-nav').height()),
      (e = 'sticky animated fadeInDown')
  }
  $(window).scrollTop() > t
    ? ($('.sticky-nav').addClass(e),
      e == 'sticky' &&
        $('.page-container').css('padding-top', $('.sticky-nav').height()))
    : ($('.sticky-nav').removeClass(e).removeAttr('style'),
      $('.page-container').removeAttr('style'))
}

function hideAll() {
  $('.animated').each(function (t) {
    $(this).closest('.hero').length ||
      $(this).removeClass('animated').addClass('hideMe')
  })
}

function inViewCheck() {
  $($('.hideMe').get().reverse()).each(function (t) {
    const e = jQuery(this)
    let i = e.offset().top + e.height()
    const a = $(window).scrollTop() + $(window).height()
    if ((e.height() > $(window).height() && (i = e.offset().top), i < a)) {
      const o = e.attr('class').replace('hideMe', 'animated')
      e.css('visibility', 'hidden').removeAttr('class'),
        setTimeout(() => {
          e.attr('class', o).css('visibility', 'visible')
        }, 0.01)
    }
  })
}

function scrollToTopView() {
  $(window).scrollTop() > $(window).height() / 3
    ? $('.scrollToTop').hasClass('showScrollTop') ||
      $('.scrollToTop').addClass('showScrollTop')
    : $('.scrollToTop').removeClass('showScrollTop')
}

function setUpVisibilityToggle() {
  $(document).on('click', '[data-toggle-visibility]', function (t) {
    t.preventDefault()
    const e = $(this).attr('data-toggle-visibility')
    if (e.indexOf(',') != -1) {
      const i = e.split(',')
      $.each(i, (t) => {
        a($(`#${i[t]}`))
      })
    } else a($(`#${e}`))

    function a(t) {
      t.is('img') ? t.toggle() : t.slideToggle()
    }
  })
}

function setUpLightBox() {
  window.targetLightbox,
    $(document)
      .on('click', '[data-lightbox]', function (t) {
        t.preventDefault(), (targetLightbox = $(this))
        const e = targetLightbox.attr('data-lightbox')
        const i = targetLightbox.attr('data-autoplay')
        let a = `<p class="lightbox-caption">${targetLightbox.attr(
          'data-caption'
        )}</p>`
        let o = 'no-gallery-set'
        const l = targetLightbox.attr('data-frame')
        targetLightbox.attr('data-gallery-id') &&
          (o = targetLightbox.attr('data-gallery-id')),
          targetLightbox.attr('data-caption') || (a = '')
        let n = ''
        i == 1 && (n = 'autoplay')
        const s = $(
          `<div id="lightbox-modal" class="modal fade"><div class="modal-dialog"><div class="modal-content ${l} blocs-lb-container"><button id="blocs-lightbox-close-btn" type="button" class="close-lightbox" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><div class="modal-body"><a href="#" class="prev-lightbox" aria-label="prev"><span class="fa fa-chevron-left"></span></a><a href="#" class="next-lightbox" aria-label="next"><span class="fa fa-chevron-right"></span></a><img id="lightbox-image" class="img-responsive" src="${e}"><div id="lightbox-video-container" class="embed-responsive embed-responsive-16by9"><video controls ${n} class="embed-responsive-item"><source id="lightbox-video" src="${e}" type="video/mp4"></video></div>${a}</div></div></div></div>`
        )
        $('body').append(s),
          l == 'fullscreen-lb' &&
            ($('#lightbox-modal')
              .addClass('fullscreen-modal')
              .append(
                '<a class="close-full-screen-modal animated fadeIn" style="animation-delay:0.5s;" onclick="$(\'#lightbox-modal\').modal(\'hide\');"><div class="close-icon"></div></a>'
              ),
            $('#blocs-lightbox-close-btn').remove()),
          e.substring(e.length - 4) == '.mp4'
            ? ($('#lightbox-image, .lightbox-caption').hide(),
              $('#lightbox-video-container').show())
            : ($('#lightbox-image,.lightbox-caption').show(),
              $('#lightbox-video-container').hide()),
          $('#lightbox-modal').modal('show'),
          o == 'no-gallery-set'
            ? ($('a[data-lightbox]').index(targetLightbox) == 0 &&
                $('.prev-lightbox').hide(),
              $('a[data-lightbox]').index(targetLightbox) ==
                $('a[data-lightbox]').length - 1 && $('.next-lightbox').hide())
            : ($(`a[data-gallery-id="${o}"]`).index(targetLightbox) == 0 &&
                $('.prev-lightbox').hide(),
              $(`a[data-gallery-id="${o}"]`).index(targetLightbox) ==
                $(`a[data-gallery-id="${o}"]`).length - 1 &&
                $('.next-lightbox').hide()),
          addLightBoxSwipeSupport()
      })
      .on('hidden.bs.modal', '#lightbox-modal', () => {
        $('#lightbox-modal').remove()
      }),
    $(document).on('click', '.next-lightbox, .prev-lightbox', function (t) {
      t.preventDefault()
      let e = 'no-gallery-set'
      let i = $('a[data-lightbox]').index(targetLightbox)
      let a = $('a[data-lightbox]').eq(i + 1)
      targetLightbox.attr('data-gallery-id') &&
        ((e = targetLightbox.attr('data-gallery-id')),
        (i = $(`a[data-gallery-id="${e}"]`).index(targetLightbox)),
        (a = $(`a[data-gallery-id="${e}"]`).eq(i + 1))),
        $(this).hasClass('prev-lightbox') &&
          ((a = $(`a[data-gallery-id="${e}"]`).eq(i - 1)),
          e == 'no-gallery-set' && (a = $('a[data-lightbox]').eq(i - 1)))
      const o = a.attr('data-lightbox')
      if (o.substring(o.length - 4) == '.mp4') {
        let l = ''
        a.attr('data-autoplay') == 1 && (l = 'autoplay'),
          $('#lightbox-image, .lightbox-caption').hide(),
          $('#lightbox-video-container')
            .show()
            .html(
              `<video controls ${l} class="embed-responsive-item"><source id="lightbox-video" src="${o}" type="video/mp4"></video>`
            )
      } else $('#lightbox-image').attr('src', o).show(), $('.lightbox-caption').html(a.attr('data-caption')).show(), $('#lightbox-video-container').hide()
      ;(targetLightbox = a),
        $('.next-lightbox, .prev-lightbox').hide(),
        e == 'no-gallery-set'
          ? ($('a[data-lightbox]').index(a) !=
              $('a[data-lightbox]').length - 1 && $('.next-lightbox').show(),
            $('a[data-lightbox]').index(a) > 0 && $('.prev-lightbox').show())
          : ($(`a[data-gallery-id="${e}"]`).index(a) !=
              $(`a[data-gallery-id="${e}"]`).length - 1 &&
              $('.next-lightbox').show(),
            $(`a[data-gallery-id="${e}"]`).index(a) > 0 &&
              $('.prev-lightbox').show())
    })
}

function addSwipeSupport() {
  $('.carousel-inner').length &&
    $('.carousel-inner').swipe({
      swipeLeft(t, e, i, a, o) {
        $(this).parent().carousel('next')
      },
      swipeRight() {
        $(this).parent().carousel('prev')
      },
      threshold: 0,
    })
}

function addKeyBoardSupport() {
  $(window).keydown((t) => {
    t.which == 37
      ? $('.prev-lightbox').is(':visible') && $('.prev-lightbox').click()
      : t.which == 39 &&
        $('.next-lightbox').is(':visible') &&
        $('.next-lightbox').click()
  })
}

function addLightBoxSwipeSupport() {
  $('#lightbox-image').length &&
    $('#lightbox-image').swipe({
      swipeLeft(t, e, i, a, o) {
        $('.next-lightbox').is(':visible') && $('.next-lightbox').click()
      },
      swipeRight() {
        $('.prev-lightbox').is(':visible') && $('.prev-lightbox').click()
      },
      threshold: 0,
    })
}
$(document).ready(() => {
  $('#scroll-hero').click((t) => {
    t.preventDefault(),
      $('html,body').animate(
        {
          scrollTop: $('#scroll-hero').closest('.bloc').height(),
        },
        'slow'
      )
  }),
    extraNavFuncs(),
    setUpSpecialNavs(),
    setUpDropdownSubs(),
    setUpLightBox(),
    setUpVisibilityToggle(),
    addSwipeSupport(),
    addKeyBoardSupport(),
    navigator.userAgent.indexOf('Safari') != -1 &&
      navigator.userAgent.indexOf('Chrome') == -1 &&
      $('#page-loading-blocs-notifaction').remove()
}),
  $(window)
    .load(() => {
      setFillScreenBlocHeight(),
        animateWhenVisible(),
        $('#page-loading-blocs-notifaction').remove()
    })
    .resize(() => {
      setFillScreenBlocHeight()
    }),
  $(() => {
    $('[data-toggle="tooltip"]').tooltip()
  })
