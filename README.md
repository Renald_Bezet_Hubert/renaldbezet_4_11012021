# **La chouette agence**

![logo](img/nom_site_agence.jpg)

## _La chouette agence, Webdesign et communication pour optimiser votre référencement naturel et concevoir des sites qui vous ressemblent_

### Optimisation du site

Après un audit, différents points ont été modifiés.

1. Amélioration au niveau du SEO

- Mises à jour sur pratiques "Black hat" en "White hat"
- Renommer le fichier `page2.html` en `contact.html`
- Ajout des mots-clés, description et titres des pages html
- mise en pratique de `strong` sur mots-clé
- changement de image en texte
- stylisation de la page contact (ex page2)

2. Amélioration au niveau de la performance 📈

- Ajout de .htaccess pour pour éviter la mise en place du cache
- Exportation des fichiers .png, . bmp,.. en .jpg
- Optimisation de la taille avec [ImageOptim](https://imageoptim.com/fr) sur Mac. Télécharger puis dans le terminal aller sur votre dossier ( `cd ~/file`). Taper :  
  `open -a ImageOptim .`ou `open -a ImageOptim *.ext`
- Enlever les texte-images du site et implémenter du texte en html.
- S'assurer des bonnes pratiques en Html et CSS [^1]

3. Prise en compte de l'accessibilité 🔍

- Augmentation de la taille de police
- Modification de la charte graphique pour assurer le contraste selon les normes [WCAG](https://www.w3.org/WAI/WCAG21/quickref/). <br/>
  Primaire: `#171313` ==> black <br/>
  Secondaire: `#ffffff` ==> white <br/>
- Ajout de balises `<aria>` sur icones des réseaux sociaux pour la description.

4. Pour la suite du développement 👨‍💻

- Ajout de `README.md` pour la suite du projet
- Ajout de `.gitignore`
- Ajout de `.gitlab-ci.yml` pour test en ligne

[^1]:
    [w3c validator](https://validator.w3.org/) <br>
    [résultat-index](https://validator.w3.org/nu/?doc=https%3A%2F%2Frenald_bezet_hubert.gitlab.io%2Frenaldbezet_4_11012021%2Findex.html)
    [résultat-contact](https://validator.w3.org/nu/?doc=https%3A%2F%2Frenald_bezet_hubert.gitlab.io%2Frenaldbezet_4_11012021%2Fcontact.html)
